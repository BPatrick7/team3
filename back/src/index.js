import app from "./app";

import { authenticateUser, logout, isLoggedIn } from "./auth";

const start = async () => {
  const controller = await initializeDatabase();

  app.get("/", (req, res, next) => res.send("ok"));

  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();
